#pragma once
#ifndef EXC_BUFFERLIST_H
#define EXC_BUFFERLIST_H
#include "List.h"
class BufferList : public List
{
private:
	Node* head;
	int size;

public:
	friend class BufferListIterator;
	class BufferListIterator : public Iterator {
	private:
		BufferList *list;
		Node * nowPointer;
	public:
		explicit BufferListIterator(BufferList *list) {
			this->list = list;
			this->nowPointer = list->head;
		}
		void start() override {
			nowPointer = list->head;
		}
		T getElement() const override {
			if (nowPointer == list->head) {
				throw BufferException();
			}
			return nowPointer->data;
		}
		void next() override {
			nowPointer = nowPointer->next;
		}
		void prev() override {
			nowPointer = nowPointer->prev;
		}
		bool isFinish() const override {
			return nowPointer->next == list->head;
		}
		Node* getNow() const override {
			return nowPointer;
		}
	};
	void pastElem(const T &elem, Iterator &iter) override {
		size++;
		Node *tempNode = new Node(elem, iter.getNow()->next, iter.getNow());
		iter.getNow()->next->prev = tempNode;
		iter.getNow()->next = tempNode;
	}
	void deleteElem(Iterator &iter) override {
		if (iter.getNow() == head) {
			throw BufferException();
		}
		Node *delElem = iter.getNow();
		iter.next();
		delElem->next->prev = delElem->prev;
		delElem->prev->next = delElem->next;
		delete delElem;
		size--;
	}
	void makeEmpty() override {
		head = head->next;
		for (int i = 0; i < size; i++) {
			head = head->next;
			delete head->prev;
		}
		head->next = head;
		head->prev = head;
		size = 0;
	}
	bool isEmpty() const override {
		return size == 0;
	}
	int getSize() const override {
		return size;
	}
	Iterator* findElem(const T &elem) override {
		auto *listIterator = new BufferListIterator(this);
		listIterator->next();
		while (listIterator->getElement() != elem && !listIterator->isFinish()) {
			listIterator->next();
		}
		if (listIterator->getElement() != elem) {
			throw NoElemException();
		}
		return listIterator;
	}
	Iterator* getIteratorFirstElem() override {
		return new BufferListIterator(this);
	}
	
	BufferList() {
		size = 0;
		head = new Node(0);
		head->next = head;
		head->prev = head;
	}
	BufferList(const BufferList& copy){
		size = copy.size;
		head = new Node(0);
		head->next = head;
		head->prev = head;
		Node *countCopy = copy.head->next;
		Node *temp = head;
		for (int i = 1; i < size; i++) {
			temp->next = new Node(countCopy->data);
			temp->next->prev = head;
			temp = temp->next;
			countCopy = countCopy->next;
		}
		temp->next = head;
		head->prev = temp;
	}
	BufferList(BufferList&& copy)  noexcept {
		size = copy.size;
		std::swap(head, copy.head);
	}
	
    BufferList& operator= (const BufferList &copy) {
	if (this == &copy) {
		return *this;
	}
	makeEmpty();
	Node *countCopy = copy.head->next;
	Node *temp = head;
	size = copy.size;
	for (int i = 1; i < size; i++) {
		temp->next = new Node(countCopy->data);
		temp->next->prev = temp;
		temp = temp->next;
		countCopy = countCopy->next;
	}
	temp->next = head;
	head->prev = temp;
	return *this;
}
BufferList& operator= (BufferList&& copy) noexcept {
	makeEmpty();
	size = copy.size;
	std::swap(head, copy.head);
	delete copy.head;
	return *this;
}
~BufferList() {
	makeEmpty();
	delete head;
}
};

#endif